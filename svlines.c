//#!/usr/bin/tcc -run
#include <unistd.h>
#include <stdlib.h>

int main (int argc, char** argv){
 unsigned int lines= argc==1? 1: atoi (argv[1]);
 char c;
 for (int i= 0,j=0; i<lines&&read (0, &c, 1); j++){
  c==0x0a? i++, j= 0: 0;
  write (1, &c, 1);
 }
 return 0;
}
