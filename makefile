cc=gcc
cf=-march=native -mtune=native -O2
af=-masm=intel -S
./svlines:	./svlines.c
	$(cc) $(cf) ./svlines.c -o ./svlines
asm:	./svlines
	$(cc) $(cf) $(af) ./svlines.c -o ./svlines.s
clean:
	rm ./svlines
cleanasm:
	rm ./svlines.s
install:
	sudo cp ./svlines /usr/bin
uninstall:
	sudo rm /usr/bin/svlines 
